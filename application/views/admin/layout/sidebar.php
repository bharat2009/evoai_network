<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
	<section class="sidebar">
		<div class="user-panel">
			<div class="pull-left image">
                <img src="<?php echo base_url();?>webroot/admin/dist/img/avatar3.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Hello, 
					<?php 
						echo $this->user;
					?>
				</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <ul class="sidebar-menu">
			<li class="<?php echo ($this->uri->segment(2)=='dashboard')?'active':''?>">
				<a href="<?php echo base_url();?>admin/dashboard">
					<i class="fa fa-dashboard"></i> <span>Dashboard</span>
				</a>
			</li>
			<li class="<?php echo ($this->uri->segment(2)=='users')?'active':''?>">
				<a href="<?php echo base_url();?>admin/users">
					<i class="fa fa-users"></i> <span>Mix Order Form: Users</span>
				</a>
			</li>
			<li class="<?php echo ($this->uri->segment(2)=='evotValue')?'active':''?>">
				<a href="<?php echo base_url();?>admin/evotValue">
					<i class="fa fa-users"></i> <span>Evot-value</span>
				</a>
			</li>
			<li class="<?php echo ($this->uri->segment(2)=='bonus')?'active':''?>">
				<a href="<?php echo base_url();?>admin/bonus">
					<i class="fa fa-users"></i> <span>Bonus value</span>
				</a>
			</li>
			<!--
			<li class="treeview <?php echo (($this->uri->segment(2)=='subscription')||($this->uri->segment(2)=='customSubscription'))?'active':''?>">
				<a href="#">
					<i class="fa fa-th-list"></i> <span>Subscription</span>
					<i class="fa pull-right fa-angle-left"></i>
				</a>
				<ul class="treeview-menu" >
					<li class="<?php echo ($this->uri->segment(2)=='subscription')?'active':''?>">
						<a href="<?php echo base_url();?>admin/subscription">
							<i class="fa fa-tag"></i> <span>Subscription</span>
						</a>
					</li>
					<li class="<?php echo ($this->uri->segment(2)=='customSubscription')?'active':''?>">
						<a href="<?php echo base_url();?>admin/customSubscription">
							<i class="fa fa-tag"></i> <span>Custom subscription</span>
						</a>
					</li>
				</ul>
			</li>
			-->
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>