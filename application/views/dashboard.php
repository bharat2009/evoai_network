<section class="header dash-header" id="top">
	<div class="container-fluid">
		<div class="header__logo pull-left">
			<a href="<?php echo base_url();?>">
				<img src="<?php echo base_url();?>webroot/frontend/images/logo_web.png" alt="" class="logo_pc">
				<img src="<?php echo base_url();?>webroot/frontend/images/logo.png" alt="" class="logo_mob">
			</a>
		</div>
		<a href="#" data-toggle="offcanvas" class="toggle-btn"><i class="fa fa-navicon fa-3x"></i></a>            
		<!--<a onclick="" href="#" target="_blank" class="btn btn-danger header__contribute">CONTRIBUTE</a>-->            
	</div>
</section>       
<section class="dashboard-section">	
	<div class="wrapper">
		<div class="row row-offcanvas row-offcanvas-left">
			<!-- sidebar -->
			<?php echo $this->load->view('sidebar.php');?>
			<!-- /sidebar -->
			<!-- main right col -->
			<div class="column col-sm-9 col-xs-11 main-dashcontent" id="main">  
				<h2 class="pro-heading mb-70">
					Dashboard
				</h2>
				<div id="msg_div">
					<?php echo $this->session->flashdata('message');?>				
				</div>
				<div class="row form-group">
					<div class="col-md-8">
						<label>Referral:</label> <input type="text" class="form-control" value="<?php echo base_url('registration?e='.$user_details->user_referral_code); ?>" id="referralLink" onclick="containtCopy();">
					</div>
					<div class="col-md-3">
						<span class="btn-submit" onclick="containtCopy();">Copy</span>
					</div>
				</div>
			</div>
			<!-- /main -->
		</div>
	</div>
</section>


