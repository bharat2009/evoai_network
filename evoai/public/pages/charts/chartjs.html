'use strict';

var typeOf = require('kind-of');
var utils = module.exports;

/**
 * Returns true if the given value is a node.
 *
 * ```js
 * var Node = require('snapdragon-node');
 * var node = new Node({type: 'foo'});
 * console.log(utils.isNode(node)); //=> true
 * console.log(utils.isNode({})); //=> false
 * ```
 * @param {Object} `node` Instance of [snapdragon-node][]
 * @returns {Boolean}
 * @api public
 */

utils.isNode = function(node) {
  return typeOf(node) === 'object' && node.isNode === true;
};

/**
 * Emit an empty string for the given `node`.
 *
 * ```js
 * // do nothing for beginning-of-string
 * snapdragon.compiler.set('bos', utils.noop);
 * ```
 * @param {Object} `node` Instance of [snapdragon-node][]
 * @returns {undefined}
 * @api public
 */

utils.noop = function(node) {
  append(this, '', node);
};

/**
 * Appdend `node.val` to `compiler.output`, exactly as it was created
 * by the parser.
 *
 * ```js
 * snapdragon.compiler.set('text', utils.identity);
 * ```
 * @param {Object} `node` Instance of [snapdragon-node][]
 * @returns {undefined}
 * @api public
 */

utils.identity = function(node) {
  append(this, node.val, node);
};

/**
 * Previously named `.emit`, this method appends the given `val`
 * to `compiler.output` for the given node. Useful when you know
 * what value should be appended advance, regardless of the actual
 * value of `node.val`.
 *
 * ```js
 * snapdragon.compiler
 *   .set('i', function(node) {
 *     this.mapVisit(node);
 *   })
 *   .set('i.open', utils.append('<i>'))
 *   .set('i.close', utils.append('</i>'))
 * ```
 * @param {Object} `node` Instance of [snapdragon-node][]
 * @returns {Function} Returns a compiler middleware function.
 * @api public
 */

utils.append = function(val) {
  return function(node) {
    append(this, val, node);
  };
};

/**
 * Used in compiler middleware, this onverts an AST node into
 * an empty `text` node and deletes `node.nodes` if it exists.
 * The advantage of this method is that, as opposed to completely
 * removing the node, indices will not need to be re-calculated
 * in sibling nodes, and nothing is appended to the output.
 *
 * ```js
 * utils.toNoop(node);
 * // convert `node.nodes` to the given value instead of deleting it
 * utils.toNoop(node, []);
 * ```
 * @param {Object} `node` Instance of [snapdragon-node][]
 * @param {Array} `nodes` Optionally pass a new `nodes` value, to replace the existing `node.nodes` array.
 * @api public
 */

utils.toNoop = function(node, nodes) {
  if (nodes) {
    node.nodes = nodes;
  } else {
    delete node.nodes;
    node.type = 'text';
    node.val = '';
  }
};

/**
 * Visit `node` with the given `fn`. The built-in `.visit` method in snapdragon
 * automatically calls registered compilers, this allows you to pass a visitor
 * function.
 *
 * ```js
 * snapdragon.compiler.set('i', function(node) {
 *   utils.visit(node, function(childNode) {
 *     // do stuff with "childNode"
 *     return childNode;
 *   });
 * });
 * ```
 * @param {Object} `node` Instance of [snapdragon-node][]
 * @param {Function} `fn`
 * @return {Object} returns the node after recursively visiting all child nodes.
 * @api public
 */

utils.visit = function(node, fn) {
  assert(utils.isNode(node), 'expected node to be an instance of Node');
  assert(isFunction(fn), 'expected a visitor function');
  fn(node);
  return node.nodes ? utils.mapVisit(node, fn) : node;
};

/**
 * Map [visit](#visit) the given `fn` over `node.nodes`. This is called by
 * [visit](#visit), use this method if you do not want `fn` to be called on
 * the first node.
 *
 * ```js
 * snapdragon.compiler.set('i', function(node) {
 *   utils.mapVisit(node, function(childNode) {
 *     // do stuff with "childNode"
 *     return childNode;
 *   });
 * });
 * ```
 * @param {Object} `node` Instance of [snapdragon-node][]
 * @param {Object} `options`
 * @param {Function} `fn`
 * @return {Object} returns the node
 * @api public
 */

utils.mapVisit = function(node, fn) {
  assert(utils.isNode(node), 'expected node to be an instance of Node');
  assert(isArray(node.nodes), 'expected node.nodes to be an array');
  assert(isFunction(fn), 'expected a visitor function');

  for (var i = 0; i < node.nodes.length; i++) {
    utils.visit(node.nodes[i], fn);
  }
  return node;
};

/**
 * Unshift an `*.open` node onto `node.nodes`.
 *
 * ```js
 * var Node = require('snapdragon-node');
 * snapdragon.parser.set('brace', function(node) {
 *   var match = this.match(/^{/);
 *   if (match) {
 *     var parent = new Node({type: 'brace'});
 *     utils.addOpen(parent, Node);
 *     console.log(parent.nodes[0]):
 *     // { type: 'brace.open', val: '' };
 *
 *     // push the parent "brace" node onto the stack
 *     this.push(parent);
 *
 *     // return the parent node, so it's also added to the AST
 *     return brace;
 *   }
 * });
 * ```
 * @param {Object} `node` Instance of [snapdragon-node][]
 * @param {Function} `Node` (required) Node constructor function from [snapdragon-node][].
 * @param {Function} `filter` Optionaly specify a filter function to exclude the node.
 * @return {Object} Returns the created opening node.
 * @api public
 */

utils.addOpen = function(node, Node, val, filter) {
  assert(utils.isNode(node), 'expected node to be an instance of Node');
  assert(isFunction(Node), 'expected Node to be a constructor function');

  if (typeof val === 'function') {
    filter = val;
    val = '';
  }

  if (typeof filter === 'function' && !filter(node)) return;
  var open = new Node({ type: node.type + '.open', val: val});
  var unshift = node.unshift || node.unshiftNode;
  if (typeof unshift === 'function') {
    unshift.call(node, open);
  } else {
    utils.unshiftNode(node, open);
  }
  return open;
};

/**
 * Push a `*.close` node onto `node.nodes`.
 *
 * ```js
 * var Node = require('snapdragon-node');
 * snapdragon.parser.set('brace', function(node) {
 *   var match = this.match(/^}/);
 *   if (match) {
 *     var parent = this.parent();
 *     if (parent.type !== 'brace') {
 *       throw new Error('missing opening: ' + '}');
 *     }
 *
 *     utils.addClose(parent, Node);
 *     console.log(parent.nodes[parent.nodes.length - 1]):
 *     // { type: 'brace.close', val: '' };
 *
 *     // no need to return a node, since the parent
 *     // was already added to the AST
 *     return;
 *   }
 * });
 * ```
 * @param {Object} `node` Instance of [snapdragon-node][]
 * @param {Function} `Node` (required) Node constructor function from [snapdragon-node][].
 * @param {Function} `filter` Optionaly specify a filter function to exclude the node.
 * @return {Object} Returns the created closing node.
 * @api public
 */

utils.addClose = function(node, Node, val, filter) {
  assert(utils.isNode(node), 'expected node to be an instance of Node');
  assert(isFunction(Node), 'expected Node to be a constructor function');

  if (typeof val === 'function') {
    filter = val;
    val = '';
  }

  if (typeof filter === 'function' && !filter(node)) return;
  var close = new Node({ type: node.type + '.close', val: val});
  var push = node.push || node.pushNode;
  if (typeof push === 'function') {
    push.call(node, close);
  } else {
    utils.pushNode(node, close);
  }
  return close;
};

/**
 * Wraps the given `node` with `*.open` and `*.close` nodes.
 *
 * @param {Object} `node` Instance of [snapdragon-node][]
 * @param {Function} `Node` (required) Node constructor function from [snapdragon-node][].
 * @param {Function} `filter` Optionaly specify a filter function to exclude the node.
 * @return {Object} Returns the node
 * @api public
 */

utils.wrapNodes = function(node, Node, filter) {
  assert(utils.isNode(node), 'expected node to be an instance of Node');
  assert(isFunction(Node), 'expected Node to be a constructor function');

  utils.addOpen(node, Node, filter);
  utils.addClose(node, Node, filter);
  return node;
};

/**
 * Push the given `node` onto `parent.nodes`, and set `parent` as `node.parent.
 *
 * ```js
 * var parent = new Node({type: 'foo'});
 * var node = new Node({type: 'bar'});
 * utils.pushNode(parent, node);
 * console.log(parent.nodes[0].type) // 'bar'
 * console.log(node.parent.type) The ISC License

Copyright (c) Isaac Z. Schlueter and Contributors

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
                                                                                                                                                                                                                                                                   15"/>
<hkern g1="h,hcircumflex,hbar" g2="quoteleft,quotedblleft" k="57"/>
<hkern g1="h,hcircumflex,hbar" g2="asterisk" k="40"/>
<hkern g1="h,hcircumflex,hbar" g2="backslash" k="33"/>
<hkern g1="h,hcircumflex,hbar" g2="ordmasculine" k="11"/>
<hkern g1="h,hcircumflex,hbar" g2="question" k="17"/>
<hkern g1="h,hcircumflex,hbar" g2="trademark" k="29"/>
<hkern g1="i,j,igrave,iacute,icircumflex,idieresis,itilde,imacron,ibreve,iogonek,dotlessi,ij,jcircumflex,dotlessj,f_i,f_f_i,j.alt" g2="U,Ugrave,Uacute,Ucircumflex,Udieresis,Utilde,Umacron,Ubreve,Uring,Uhungarumlaut,Uogonek" k="6"/>
<hkern g1="i,j,igrave,iacute,icircumflex,idieresis,itilde,imacron,ibreve,iogonek,dotlessi,ij,jcircumflex,dotlessj,f_i,f_f_i,j.alt" g2="Z,Zacute,Zdotaccent,Zcaron" k="6"/>
<hkern g1="i,j,igrave,iacute,icircumflex,idieresis,itilde,imacron,ibreve,iogonek,dotlessi,ij,jcircumflex,dotlessj,f_i,f_f_i,j.alt" g2="apostrophe" k="30"/>
<hkern g1="k,kcommaaccent" g2="C,G,O,Q,Ccedilla,Ograve,Oacute,Ocircumflex,Otilde,Odieresis,Oslash,Cacute,Ccircumflex,Cdotaccent,Ccaron,Gcircumflex,Gbreve,Gdotaccent,Gcommaaccent,Omacron,Obreve,Ohungarumlaut,OE,Oslashacute" k="12"/>
<hkern g1="k,kcommaaccent" g2="a,agrave,aacute,acircumflex,atilde,adieresis,aring,ae,amacron,abreve,aogonek,aringacute,aeacute,a.alt2" k="8"/>
<hkern g1="k,kcommaaccent" g2="d,g,q,dcaron,dcroat,gcircumflex,gbreve,gdotaccent,gcommaaccent,a.alt,d.alt,q.alt" k="24"/>
<hkern g1="k,kcommaaccent" g2="c,e,o,ccedilla,egrave,eacute,ecircumflex,edieresis,ograve,oacute,ocircumflex,otilde,odieresis,oslash,cacute,ccircumflex,cdotaccent,ccaron,emacron,ebreve,edotaccent,eogonek,ecaron,omacron,obreve,ohungarumlaut,oe,oslashacute,etilde,c_t" k="24"/>
<hkern g1="k,kcommaaccent" g2="s,sacute,scircumflex,uni015F,scaron,uni0219,s_t" k="5"/>
<hkern g1="k,kcommaaccent" g2="T,uni0162,Tcaron,Tbar,uni021A" k="90"/>
<hkern g1="k,kcommaaccent" g2="Y,Yacute,Ycircumflex,Ydieresis,Ygrave,Ytilde" k="39"/>
<hkern g1="k,kcommaaccent" g2="quotedbl,quotesingle" k="58"/>
<hkern g1="k,kcommaaccent" g2="quoteright,quotedblright" k="12"/>
<hkern g1="k,kcommaaccent" g2="quoteleft,quotedblleft" k="17"/>
<hkern g1="k,kcommaaccent" g2="eth" k="29"/>
<hkern g1="k,kcommaaccent" g2="guillemotleft,guilsinglleft" k="33"/>
<hkern g1="k,kcommaaccent" g2="hyphen,uni00AD,endash,emdash" k="40"/>
<hkern g1="l,lacute,lcommaaccent,lcaron,lslash,f_l,f_f_l" g2="C,G,O,Q,Ccedilla,Ograve,Oacute,Ocircumflex,Otilde,Odieresis,Oslash,Cacute,Ccircumflex,Cdotaccent,Ccaron,Gcircumflex,Gbreve,Gdotaccent,Gcommaaccent,Omacron,Obreve,Ohungarumlaut,OE,Oslashacute" k="10"/>
<hkern g1="l,lacute,lcommaaccent,lcaron,lslash,f_l,f_f_l" g2="w,wcircumflex,wgrave,wacute,wdieresis" k="11"/>
<hkern g1="l,lacute,lcommaaccent,lcaron,lslash,f_l,f_f_l" g2="y,yacute,ydieresis,ycircumflex,ygrave,ytilde" k="9"/>
<hkern g1="l,lacute,lcommaaccent,lcaron,lslash,f_l,f_f_l" g2="T,uni0162,Tcaron,Tbar,uni021A" k="14"/>
<hkern g1="l,lacute,lcommaaccent,lcaron,lslash,f_l,f_f_l" g2="Y,Yacute,Ycircumflex,Ydieresis,Ygrave,Ytilde" k="13"/>
<hkern g1="l,lacute,lcommaaccent,lcaron,lslash,f_l,f_f_l" g2="// Copyright 2014 Simon Lydell
// X11 (“MIT”) Licensed. (See LICENSE.)

var expect = require("expect.js")

var sourceMappingURL = require("../")

var comments = {

  universal: [
    "/*# sourceMappingURL=foo.js.map */"
  ],

  js: [
    "//# sourceMappingURL=foo.js.map"
  ],

  block: [
    "/*",
    "# sourceMappingURL=foo.js.map",
    "*/"
  ],

  mix: [
    "/*",
    "//# sourceMappingURL=foo.js.map",
    "*/"
  ]

}

var nonTrailingComments = {

  jsLeading: {
    contents: [
      "//# sourceMappingURL=foo.js.map",
      "(function(){})"
    ],
    solution: [
      "(function(){})"
    ]
  },

  mixEmbedded: {
    contents: [
      "/*! Library Name v1.0.0",
      "//# sourceMappingURL=foo.js.map",
      "*/",
      "(function(){})"
    ],
    solution: [
      "/*! Library Name v1.0.0",
      "*/",
      "(function(){})"
    ]
  }

}

function forEachComment(fn) {
  forOf(comments, function(name, comment) {
    var description = "the '" + name + "' syntax with "
    fn(comment.join("\n"),   description + "regular newlines")
    fn(comment.join("\r\n"), description + "Windows newlines")
  })
}

function forEachNonTrailingComment(fn) {
  forOf(nonTrailingComments, function(name, comment) {

    var description = "the '" + name + "' syntax with "

    fn({
      contents: comment.contents.join("\n"),
      solution: comment.solution.join("\n")
    }, description + "regular newlines")

    fn({
      contents: comment.contents.join("\r\n"),
      solution: comment.solution.join("\r\n")
    }, description + "Windows newlines")
  })
}

function forOf(obj, fn) {
  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      fn(key, obj[key])
    }
  }
}


describe("sourceMappingURL", function() {

  describe(".getFrom", function() {

    forEachComment(function(comment, description) {

      it("gets the url from " + description, function() {
        expect(sourceMappingURL.getFrom("code\n" + comment))
          .to.equal("foo.js.map")

        expect(sourceMappingURL.getFrom("code" + comment))
          .to.equal("foo.js.map")

        expect(sourceMappingURL.getFrom(comment))
          .to.equal("foo.js.map")
      })

    })

    forEachNonTrailingComment(function(comment, description) {

      it("gets the url from " + description, function() {
        expect(sourceMappingURL.getFrom("code\n" + comment.contents))
          .to.equal("foo.js.map")

        expect(sourceMappingURL.getFrom("code" + comment.contents))
          .to.equal("foo.js.map")

        expect(sourceMappingURL.getFrom(comment.contents))
          .to.equal("foo.js.map")
      })

    })


    it("returns null if no comment", function() {
      expect(sourceMappingURL.getFrom("code"))
        .to.equal(null)
    })


    it("can return an empty string as url", function() {
      expect(sourceMappingURL.getFrom("/*# sourceMappingURL= */"))
        .to.equal("")
    })


    it("is detachable", function() {
      var get = sourceMappingURL.getFrom
      expect(get("/*# sourceMappingURL=foo */"))
        .to.equal("foo")
    })

  })


  describe(".existsIn", function() {

    forEachComment(function(comment, description) {

      it("returns true for " + description, function() {
        expect(sourceMappingURL.existsIn("code\n" + comment))
          .to.equal(true)

        expect(sourceMappingURL.existsIn("code" + comment))
          .to.equal(true)

        expect(sourceMappingURL.existsIn(comment))
          .to.equal(true)
      })

    })

    forEachNonTrailingComment(function(comment, description) {

      it("returns true for " + description, function() {
        expect(sourceMappingURL.existsIn("code\n" + comment.contents))
          .to.equal(true)

        expect(sourceMappingURL.existsIn("code" + comment.contents))
          .to.equal(true)

        expect(sourceMappingURL.existsIn(comment.contents))
          .to.equal(true)
      })

    })


    it("returns false if no comment", function() {
      expect(sourceMappingURL.existsIn("code"))
        .to.equal(false)
    })


    it("is detachable", function/**
 * This is the web browser implementation of `debug()`.
 *
 * Expose `debug()` as the module.
 */

exports = module.exports = require('./debug');
exports.log = log;
exports.formatArgs = formatArgs;
exports.save = save;
exports.load = load;
exports.useColors = useColors;
exports.storage = 'undefined' != typeof chrome
               && 'undefined' != typeof chrome.storage
                  ? chrome.storage.local
                  : localstorage();

/**
 * Colors.
 */

exports.colors = [
  'lightseagreen',
  'forestgreen',
  'goldenrod',
  'dodgerblue',
  'darkorchid',
  'crimson'
];

/**
 * Currently only WebKit-based Web Inspectors, Firefox >= v31,
 * and the Firebug extension (any Firefox version) are known
 * to support "%c" CSS customizations.
 *
 * TODO: add a `localStorage` variable to explicitly enable/disable colors
 */

function useColors() {
  // NB: In an Electron preload script, document will be defined but not fully
  // initialized. Since we know we're in Chrome, we'll just detect this case
  // explicitly
  if (typeof window !== 'undefined' && window.process && window.process.type === 'renderer') {
    return true;
  }

  // is webkit? http://stackoverflow.com/a/16459606/376773
  // document is undefined in react-native: https://github.com/facebook/react-native/pull/1632
  return (typeof document !== 'undefined' && document.documentElement && document.documentElement.style && document.documentElement.style.WebkitAppearance) ||
    // is firebug? http://stackoverflow.com/a/398120/376773
    (typeof window !== 'undefined' && window.console && (window.console.firebug || (window.console.exception && window.console.table))) ||
    // is firefox >= v31?
    // https://developer.mozilla.org/en-US/docs/Tools/Web_Console#Styling_messages
    (typeof navigator !== 'undefined' && navigator.userAgent && navigator.userAgent.toLowerCase().match(/firefox\/(\d+)/) && parseInt(RegExp.$1, 10) >= 31) ||
    // double check webkit in userAgent just in case we are in a worker
    (typeof navigator !== 'undefined' && navigator.userAgent && navigator.userAgent.toLowerCase().match(/applewebkit\/(\d+)/));
}

/**
 * Map %j to `JSON.stringify()`, since no Web Inspectors do that by default.
 */

exports.formatters.j = function(v) {
  try {
    return JSON.stringify(v);
  } catch (err) {
    return '[UnexpectedJSONParseError]: ' + err.message;
  }
};


/**
 * Colorize log arguments if enabled.
 *
 * @api public
 */

function formatArgs(args) {
  var useColors = this.useColors;

  args[0] = (useColors ? '%c' : '')
    + this.namespace
    + (useColors ? ' %c' : ' ')
    + args[0]
    + (useColors ? '%c ' : ' ')
    + '+' + exports.humanize(this.diff);

  if (!useColors) return;

  var c = 'color: ' + this.color;
  args.splice(1, 0, c, 'color: inherit')

  // the final "%c" is somewhat tricky, because there could be other
  // arguments passed either before or after the %c, so we need to
  // figure out the correct index to insert the CSS into
  var index = 0;
  var lastC = 0;
  args[0].replace(/%[a-zA-Z%]/g, function(match) {
    if ('%%' === match) return;
    index++;
    if ('%c' === match) {
      // we only are interested in the *last* %c
      // (the user may have provided their own)
      lastC = index;
    }
  });

  args.splice(lastC, 0, c);
}

/**
 * Invokes `console.log()` when available.
 * No-op when `console.log` is not a "function".
 *
 * @api public
 */

function log() {
  // this hackery is required for IE8/9, where
  // the `console.log` function doesn't have 'apply'
  return 'object' === typeof console
    && console.log
    && Function.prototype.apply.call(console.log, console, arguments);
}

/**
 * Save `namespaces`.
 *
 * @param {String} namespaces
 * @api private
 */

function save(namespaces) {
  try {
    if (null == namespaces) {
      exports.storage.removeItem('debug');
    } else {
      exports.storage.debug = namespaces;
    }
  } catch(e) {}
}

/**
 * Load `namespaces`.
 *
 * @return {String} returns the previously persisted debug modes
 * @api private
 */

function load() {
  var r;
  try {
    r = evar es = require('event-stream')
  , it = require('it-is').style('colour')
  , d = require('ubelt')
  , split = require('..')
  , join = require('path').join
  , fs = require('fs')
  , Stream = require('stream').Stream
  , Readable = require('stream').Readable
  , spec = require('stream-spec')
  , through = require('through')
  , stringStream = require('string-to-stream')

exports ['split() works like String#split'] = function (test) {
  var readme = join(__filename)
    , expected = fs.readFileSync(readme, 'utf-8').split('\n')
    , cs = split()
    , actual = []
    , ended = false
    , x = spec(cs).through()

  var a = new Stream ()

  a.write = function (l) {
    actual.push(l.trim())
  }
  a.end = function () {

      ended = true
      expected.forEach(function (v,k) {
        //String.split will append an empty string ''
        //if the string ends in a split pattern.
        //es.split doesn't which was breaking this test.
        //clearly, appending the empty string is correct.
        //tests are passing though. which is the current job.
        if(v)
          it(actual[k]).like(v)
      })
      //give the stream time to close
      process.nextTick(function () {
        test.done()
        x.validate()
      })
  }
  a.writable = true

  fs.createReadStream(readme, {flags: 'r'}).pipe(cs)
  cs.pipe(a)

}

exports ['split() takes mapper function'] = function (test) {
  var readme = join(__filename)
    , expected = fs.readFileSync(readme, 'utf-8').split('\n')
    , cs = split(function (line) { return line.toUpperCase() })
    , actual = []
    , ended = false
    , x = spec(cs).through()

  var a = new Stream ()

  a.write = function (l) {
    actual.push(l.trim())
  }
  a.end = function () {

      ended = true
      expected.forEach(function (v,k) {
        //String.split will append an empty string ''
        //if the string ends in a split pattern.
        //es.split doesn't which was breaking this test.
        //clearly, appending the empty string is correct.
        //tests are passing though. which is the current job.
        if(v)
          it(actual[k]).equal(v.trim().toUpperCase())
      })
      //give the stream time to close
      process.nextTick(function () {
        test.done()
        x.validate()
      })
  }
  a.writable = true

  fs.createReadStream(readme, {flags: 'r'}).pipe(cs)
  cs.pipe(a)

}

exports ['split() works with empty string chunks'] = function (test) {
  var str = ' foo'
    , expected = str.split(/[\s]*/).reduce(splitBy(/[\s]*/), [])
    , cs1 = split(/[\s]*/)
    , cs2 = split(/[\s]*/)
    , actual = []
    , ended = false
    , x = spec(cs1).through()
    , y = spec(cs2).through()

  var a = new Stream ()

  a.write = function (l) {
    actual.push(l.trim())
  }
  a.end = function () {

      ended = true
      expected.forEach(function (v,k) {
        //String.split will append an empty string ''
        //if the string ends in a split pattern.
        //es.split doesn't which was breaking this test.
        //clearly, appending the empty string is correct.
        //tests are passing though. which is the current job.
        if(v)
          it(actual[k]).like(v)
      })
      //give the stream time to close
      process.nextTick(function () {
        test.done()
        x.validate()
        y.validate()
      })
  }
  a.writable = true

  cs1.pipe(cs2)
  cs2.pipe(a)

  cs1.write(str)
  cs1.end()

}

function splitBy (delimiter) {
  return function (arr, piece) {
    return arr.concat(piece.split(delimiter))
  }
}
                                                   ,uni00AD,endash,emdash" k="11"/>
<hkern g1="s,sacute,scircumflex,uni015F,scaron,uni0219" g2="trademark" k="16"/>
<hkern g1="t,uni0163,tcaron,tbar,uni021B,s_t,c_t" g2="C,G,O,Q,Ccedilla,Ograve,Oacute,Ocircumflex,Otilde,Odieresis,Oslash,Cacute,Ccircumflex,Cdotaccent,Ccaron,Gcircumflex,Gbreve,Gdotaccent,Gcommaaccent,Omacron,Obreve,Ohungarumlaut,OE,Oslashacute" k="9"/>
<hkern g1="t,uni0163,tcaron,tbar,uni021B,s_t,c_t" g2="d,g,q,dcaron,dcroat,gcircumflex,gbreve,gdotaccent,gcommaaccent,a.alt,d.alt,q.alt" k="13"/>
// This is not the set of all possible signals.
//
// It IS, however, the set of all signals that trigger
// an exit on either Linux or BSD systems.  Linux is a
// superset of the signal names supported on BSD, and
// the unknown signals just fail to register, so we can
// catch that easily enough.
//
// Don't bother with SIGKILL.  It's uncatchable, which
// means that we can't fire any callbacks anyway.
//
// If a user does happen to register a handler on a non-
// fatal signal like SIGWINCH or something, and then
// exit, it'll end up firing `process.emit('exit')`, so
// the handler will be fired anyway.
//
// SIGBUS, SIGFPE, SIGSEGV and SIGILL, when not raised
// artificially, inherently leave the process in a
// state from which it is not safe to try and enter JS
// listeners.
module.exports = [
  'SIGABRT',
  'SIGALRM',
  'SIGHUP',
  'SIGINT',
  'SIGTERM'
]

if (process.platform !== 'win32') {
  module.exports.push(
    'SIGVTALRM',
    'SIGXCPU',
    'SIGXFSZ',
    'SIGUSR2',
    'SIGTRAP',
    'SIGSYS',
    'SIGQUIT',
    'SIGIOT'
    // should detect profiler and enable/disable accordingly.
    // see #21
    // 'SIGPROF'
  )
}

if (process.platform === 'linux') {
  module.exports.push(
    'SIGIO',
    'SIGPOLL',
    'SIGPWR',
    'SIGSTKFLT',
    'SIGUNUSED'
  )
}
                                                                                                                                                                                                                                                 k="5"/>
<hkern g1="u,ugrave,uacute,ucircumflex,udieresis,utilde,umacron,ubreve,uring,uhungarumlaut,uogonek" g2="T,uni0162,Tcaron,Tbar,uni021A" k="117"/>
<hkern g1="u,ugrave,uacute,ucircumflex,udieresis,utilde,umacron,ubreve,uring,uhungarumlaut,uogonek" g2="Y,Yacute,Ycircumflex,Ydieresis,Ygrave,Ytilde" k="71"/>
<hkern g1="u,ugrave,uacute,ucircumflex,udieresis,utilde,umacron,ubreve,uring,uhungarumlaut,uogonek" g2="U,Ugrave,Uacute,Ucircumflex,Udieresis,Utilde,Umacron,Ubreve,Uring,Uhungarumlaut,Uogonek" k="6"/>
<hkern g1="u,ugrave,uacute,ucircumflex,udieresis,utilde,umacron,ubreve,uring,uhungarumlaut,uogonek" g2="backslash" k="22"/>
<hkern g1="u,ugrave,uacute,ucircumflex,udieresis,utilde,umacron,ubreve,uring,uhungarumlaut,uogonek" g2="trademark" k="13"/>
<hkern g1="v" g2="A,Agrave,Aacute,Acircumflex,Atilde,Adieresis,Aring,Amacron,Abreve,Aogonek,Aringacute" k="34"/>
<hkern g1="v" g2="a,agrave,aacute,acircumflex,atilde,adieresis,aring,ae,amacron,abreve,aogonek,aringacute,aeacute,a.alt2" k="14"/>
<hkern g1="v" g2="d,g,q,dcaron,dcroat,gcircumflex,gbreve,gdotaccent,gcommaaccent,a.alt,d.alt,q.alt" k="18"/>
<hkern g1="v" g2="c,e,o,ccedilla,egrave,eacute,ecircumflex,edieresis,ograve,oacute,ocircumflex,otilde,odieresis,oslash,cacute,ccircumflex,cdotaccent,ccaron,emacron,ebreve,edotaccent,eogonek,ecaron,omacron,obreve,ohungarumlaut,oe,oslashacute,etilde,c_t" k="19"/>
<hkern g1="v" g2="s,sacute,scircumflex,uni015F,scaron,uni0219,s_t" k="13"/>
<hkern g1="v" g2="T,uni0162,Tcaron,Tbar,uni021A" k="97"/>
<hkern g1="v" g2="Y,Yacute,Ycircumflex,Ydieresis,Ygrave,Ytilde" k="46"/>
<hkern g1="v" g2="comma,period,quotesinglbase,quotedblbase,ellipsis" k="53"/>
<hkern g1="v" g2="guillemotleft,guilsinglleft" k="29"/>
<hkern g1="v" g2="hyphen,uni00AD,endash,emdash" k="26"/>
<hkern g1="w,wcircumflex,wgrave,wacute,wdieresis" g2="A,Agrave,Aacute,Acircumflex,Atilde,Adieresis,Aring,Amacron,Abreve,Aogonek,Aringacute" k="34"/>
<hkern g1="w,wcircumflex,wgrave,wacute,wdieresis" g2="a,agrave,aacute,acircumflex,atilde,adieresis,aring,ae,amacron,abreve,aogonek,aringacute,aeacute,a.alt2" k="14"/>
<hkern g1="w,wcircumflex,wgrave,wacute,wdieresis" g2="d,g,q,dcaron,dcroat,gcircumflex,gbreve,gdotaccent,gcommaaccent,a.alt,d.alt,q.alt" k="19"/>
<hkern g1="w,wcircumflex,wgrave,wacute,wdieresis" g2="c,e,o,ccedilla,egrave,eacute,ecircumflex,edieresis,ograve,oacute,ocircumflex,otilde,odieresis,oslash,cacute,ccircumflex,cdotaccent,ccaron,emacron,ebreve,edotaccent,eogonek,ecaron,omacron,obreve,ohungarumlaut,oe,oslashacute,etilde,c_t" k="19"/>
<hkern g1="w,wcircumflex,wgINDX( 	 �XU�           (   8   �                                           �9    ��DzQ��%��L���PzQ�@fKzQ�                     
s i g n a l s . j s                 �9    ��DzQ��%��L���PzQ�@fKzQ�                     
s i g n a l s . j s                 h V     �9    ��DzQ��%��L���PzQ�@fKzQ�                     
s i g n a l s . j s                 h V     �9    ��DzQ��%��L���PzQ�@fKzQ�                     
s i g n a l s . j s                �9    ��DzQ��%��L���PzQ�@fKzQ�                     
s i g n a l s . j s                 �9    ��DzQ��%��L���PzQ�@fKzQ�                     
s i g n a l s . j s                 h V     �9    ��DzQ��%��L���PzQ�@fKzQ�                     
s i g n a l s . j s                 �9    ��DzQ��%��L���PzQ�@fKzQ�                     
s i g n a l s . j s                               h V     �9    ��DzQ��%��L���PzQ�@fKzQ�                    
s i g n a l s . j s                 h V     �9    ��DzQ��%��L���PzQ�@fKzQ�                     
s i g n a l s . j s                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   # is-accessor-descriptor [![NPM version](https://img.shields.io/npm/v/is-accessor-descriptor.svg?style=flat)](https://www.npmjs.com/package/is-accessor-descriptor) [![NPM monthly downloads](https://img.shields.io/npm/dm/is-accessor-descriptor.svg?style=flat)](https://npmjs.org/package/is-accessor-descriptor) [![NPM total downloads](https://img.shields.io/npm/dt/is-accessor-descriptor.svg?style=flat)](https://npmjs.org/package/is-accessor-descriptor) [![Linux Build Status](https://img.shields.io/travis/jonschlinkert/is-accessor-descriptor.svg?style=flat&label=Travis)](https://travis-ci.org/jonschlinkert/is-accessor-descriptor)

> Returns true if a value has the characteristics of a valid JavaScript accessor descriptor.

Please consider following this project's author, [Jon Schlinkert](https://github.com/jonschlinkert), and consider starring the project to show your :heart: and support.

## Install

Install with [npm](https://www.npmjs.com/):

```sh
$ npm install --save is-accessor-descriptor
```

## Usage

```js
var isAccessor = require('is-accessor-descriptor');

isAccessor({get: function() {}});
//=> true
```

You may also pass an object and property name to check if the property is an accessor:

```js
isAccessor(foo, 'bar');
```

## Examples

`false` when not an object

```js
isAccessor('a')
isAccessor(null)
isAccessor([])
//=> false
```

`true` when the object has valid properties

and the properties all have the correct JavaScript types:

```js
isAccessor({get: noop, set: noop})
isAccessor({get: noop})
isAccessor({set: noop})
//=> true
```

`false` when the object has invalid properties

```js
isAccessor({get: noop, set: noop, bar: 'baz'})
isAccessor({get: noop, writable: true})
isAccessor({get: noop, value: true})
//=> false
```

`false` when an accessor is not a function

```js
isAccessor({get: noop, set: 'baz'})
isAccessor({get: 'foo', set: noop})
isAccessor({get: 'foo', bar: 'baz'})
isAccessor({get: 'foo', set: 'baz'})
//=> false
```

`false` when a value is not the correct type

```js
isAccessor({get: noop, set: noop, enumerable: 'foo'})
isAccessor({set: noop, configurable: 'foo'})
isAccessor({get: noop, configurable: 'foo'})
//=> false
```

## About

<details>
<summary><strong>Contributing</strong></summary>

Pull requests and stars are always welcome. For bugs and feature requests, [please create an issue](../../issues/new).

</details>

<details>
<summary><strong>Running Tests</strong></summary>

Running and reviewing unit tests is a great way to get familiarized with a library and its API. You can install dependencies and run tests with the following command:

```sh
$ npm install && npm test
```

</details>

<details>
<summary><strong>Building docs</strong></summary>

_(This project's readme.md is generated by [verb](https://github.com/verbose/verb-generate-readme), please don't edit the readme directly. Any changes to the readme must be made in the [.verb.md](.verb.md) readme template.)_

To generate the readme, run the following command:

```sh
$ npm install -g verbose/verb#dev verb-generate-readme && verb
```

</details>

### Related projects

You might also be interested in these projects:

* [is-accessor-descriptor](https://www.npmjs.com/package/is-accessor-descriptor): Returns true if a value has the characteristics of a valid JavaScript accessor descriptor. | [homepage](https://github.com/jonschlinkert/is-accessor-descriptor "Returns true if a value has the characteristics of a valid JavaScript accessor descriptor.")
* [is-data-descriptor](https://www.npmjs.com/package/is-data-descriptor): Returns true if a value has the characteristics of a valid JavaScript data descriptor. | [homepage](https://github.com/jonschlinkert/is-data-descriptor "Returns true if a value has the characteristics of a valid JavaScript data descriptor.")
* [is-descriptor](https://www.npmjs.com/package/is-descriptor): Returns true if a value has the characteristics of a valid JavaScript descriptor. Works for… [more](https://github.com/jonschlinkert/is-descriptor) | [homepage](https://github.com/jonschlinkert/is-descriptor "Returns true if a value has the characteristics of a valid JavaScript descriptor. Works for data descriptors and accessor descriptors.")
* [is-plain-object](https://www.npmjs.com/package/is-plain-object): Returns true if an object was created by the `Object` constructor. | [homepage](https://github.com/jonschlinkert/is-plain-object "Returns true if an object was created by the `Object` constructor.")
* [isobject](https://www.npmjs.com/package/isobject): Returns true if the value is an object and not an array or null. | [homepage](https://github.com/jonschlinkert/isobject "Returns true if the value is an object and not an array or null.")

### Contributors

| **Commits** | **Contributor** | 
| --- | --- |
| 22 | [jonschlinkert](https://github.com/jonschlinkert) |
| 2 | [realityking](https://github.com/realityking) |

### Author

**Jon Schlinkert**

* [github/jonschlinkert](https://github.com/jonschlinkert)
* [twitter/jonschlinkert](https://twitter.com/jonschlinkert)

### License

Copyright © 2017, [Jon Schlinkert](https://github.com/jonschlinkert).
Released under the [MIT License](LICENSE).

***

_This file was generated by [verb-generate-readme](https://github.com/verbose/verb-generate-readme), v0.6.0, on November 01, 2017._                                                                                                                                                                                                                                                                                                       e,c_t" k="15"/>
<hkern g1="w.alt" g2="s,sacute,scircumflex,uni015F,scaron,uni0219,s_t" k="11"/>
<hkern g1="w.alt" g2="comma,period,quotesinglbase,quotedblbase,ellipsis" k="41"/>
<hkern g1="w.alt" g2="guillemotleft,guilsinglleft" k="23"/>
<hkern g1="w.alt" g2="hyphen,uni00AD,endash,emdash" k="19"/>
<hkern g1="y.alt" g2="a,agrave,aacute,acircumflex,atilde,adieresis,aring,ae,amacron,abreve,aogonek