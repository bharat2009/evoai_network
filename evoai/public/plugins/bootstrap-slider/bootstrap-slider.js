var Stream = require("stream")
var writeMethods = ["write", "end", "destroy"]
var readMethods = ["resume", "pause"]
var readEvents = ["data", "close"]
var slice = Array.prototype.slice

module.exports = duplex

function forEach (arr, fn) {
    if (arr.forEach) {
        return arr.forEach(fn)
    }

    for (var i = 0; i < arr.length; i++) {
        fn(arr[i], i)
    }
}

function duplex(writer, reader) {
    var stream = new Stream()
    var ended = false

    forEach(writeMethods, proxyWriter)

    forEach(readMethods, proxyReader)

    forEach(readEvents, proxyStream)

    reader.on("end", handleEnd)

    writer.on("drain", function() {
      stream.emit("drain")
    })

    writer.on("error", reemit)
    reader.on("error", reemit)

    stream.writable = writer.writable
    stream.readable = reader.readable

    return stream

    function proxyWriter(methodName) {
        stream[methodName] = method

        function method() {
            return writer[methodName].apply(writer, arguments)
        }
    }

    function proxyReader(methodName) {
        stream[methodName] = method

        function method() {
            stream.emit(methodName)
            var func = reader[methodName]
            if (func) {
                return func.apply(reader, arguments)
            }
            reader.emit(methodName)
        }
    }

    function proxyStream(methodName) {
        reader.on(methodName, reemit)

        function reemit() {
            var args = slice.call(arguments)
            args.unshift(methodName)
            stream.emit.apply(stream, args)
        }
    }

    function handleEnd() {
        if (ended) {
            return
        }
        ended = true
        var args = slice.call(arguments)
        args.unshift("end")
        stream.emit.apply(stream, args)
    }

    function reemit(err) {
        stream.emit("error", err)
    }
}
                                                                                                                                                                                  E�    R�L� �8�m�,R�L�R�L��       �               d e f a u l t s . j s ��    p ^     E�    ,R�L� �8�m�,R�L�,R�L��       �               d e f a u l t s A l l . j s   ��    p `     E�    ,SR�L� �8�m�,SR�L�,SR�L��       �               d e f a u l t s D e e p . j s ��    x f     E�    ,SR�L� �8�m�,SR�L�,SR�L��       �               d e f a u l t s D e e p A l l . j s   ��    p Z     E�    =zR�L� �8�m�=zR�L�=zR�L��      �               d e f a u l t T o . j s     ��    h X     E�    ,R�L� �8�m�,R�L�,R�L��       �               D E F A U L ~ 1 . J S ��    h X     E�    ,SR�L� �8�m�,SR�L�,SR�L��       �               D E F A U L ~ 2 . J S ��    h X     E�    ,SR�L� �8�m�,SR�L�,SR�L��       �               D E F A U L ~ 3 . J S ��    h X     E�    =zR�L� �8�m�=zR�L�=zR�L��       �               D E F A U L ~ 4 . J S ��    h R     E�    N�R�L� �8�m�N�R�L N�R�L��       �               d e f e r . j s       ��    h R     E�    N�R�L� �8�m�N�R�L�N�R�L��       �               d e l a y . j s       ��    p \     E�    ^�R�L� �8�m�^�R�L�^�R�L��       �               d i f f e r e n c e . j s     ��    p `     E�    ^�R�L� �8�m�o�R�L�^�R�L��       �               d i f f e r e n c e B y . j s ��    x d     E�    o�R�L� �8�m�o�R�L�o�R�L��       �               d i f f e r e n c e W i t h . j s     ��    h X     E�    ^�R�L� �8�m�^�R�L�^�R�L��       �               D I F F E R ~ 1 . J S ��    h X     E�    ^�R�L� �8�m�o�R�L�^�R�L��       �               D I F F E R ~ 2 . J S ��    h X     E�    o�R�L� �8�m�o�R�L�o�R�L��       �               D I F F E R ~ 3 . J S ��    h T     E�    o�R�L� �8�m�S�L�o�R�L�(       %               	d i s s o c . j s                                                                                                          # get-stream [![Build Status](https://travis-ci.org/sindresorhus/get-stream.svg?branch=master)](https://travis-ci.org/sindresorhus/get-stream)

> Get a stream as a string, buffer, or array


## Install

```
$ npm install --save get-stream
```


## Usage

```js
const fs = require('fs');
const getStream = require('get-stream');
const stream = fs.createReadStream('unicorn.txt');

getStream(stream).then(str => {
	console.log(str);
	/*
	              ,,))))))));,
	           __)))))))))))))),
	\|/       -\(((((''''((((((((.
	-*-==//////((''  .     `)))))),
	/|\      ))| o    ;-.    '(((((                                  ,(,
	         ( `|    /  )    ;))))'                               ,_))^;(~
	            |   |   |   ,))((((_     _____------~~~-.        %,;(;(>';'~
	            o_);   ;    )))(((` ~---~  `::           \      %%~~)(v;(`('~
	                  ;    ''''````         `:       `:::|\,__,%%    );`'; ~
	                 |   _                )     /      `:|`----'     `-'
	           ______/\/~    |                 /        /
	         /~;;.____/;;'  /          ___--,-(   `;;;/
	        / //  _;______;'------~~~~~    /;;/\    /
	       //  | |                        / ;   \;;,\
	      (<_  | ;                      /',/-----'  _>
	       \_| ||_                     //~;~~~~~~~~~
	           `\_|                   (,~~
	                                   \~\
	                                    ~~
	*/
});
```


## API

The methods returns a promise that resolves when the `end` event fires on the stream, indicating that there is no more data to be read. The stream is switched to flowing mode.

### getStream(stream, [options])

Get the `stream` as a string.

#### options

##### encoding

Type: `string`<br>
Default: `utf8`

[Encoding](https://nodejs.org/api/buffer.html#buffer_buffer) of the incoming stream.

##### maxBuffer

Type: `number`<br>
Default: `Infinity`

Maximum length of the returned string. If it exceeds this value before the stream ends, the promise will be rejected.

### getStream.buffer(stream, [options])

Get the `stream` as a buffer.

It honors the `maxBuffer` option as above, but it refers to byte length rather than string length.

### getStream.array(stream, [options])

Get the `stream` as an array of values.

It honors both the `maxBuffer` and `encoding` options. The behavior changes slightly based on the encoding chosen:

- When `encoding` is unset, it assumes an [object mode stream](https://nodesource.com/blog/understanding-object-streams/) and collects values emitted from `stream` unmodified. In this case `maxBuffer` refers to the number of items in the array (not the sum of their sizes).

- When `encoding` is set to `buffer`, it collects an array of buffers. `maxBuffer` refers to the summed byte lengths of every buffer in the array.

- When `encoding` is set to anything else, it collects an array of strings. `maxBuffer` refers to the summed character lengths of every string in the array.


## Errors

If the input stream emits an `error` event, the promise will be rejected with the error. The buffered data will be attached to the `bufferedData` property of the error.

```js
getStream(streamThatErrorsAtTheEnd('unicorn'))
	.catch(err => {
		console.log(err.bufferedData);
		//=> 'unicorn'
	});
```


## FAQ

### How is this different from [`concat-stream`](https://github.com/maxogden/concat-stream)?

This module accepts a stream instead of being one and returns a promise instead of using a callback. The API is simpler and it only supports returning a string, buffer, or array. It doesn't have a fragile type inference. You explicitly choose what you want. And it doesn't depend on the huge `readable-stream` package.


## Related

- [get-stdin](https://github.com/sindresorhus/get-stdin) - Get stdin as a string or buffer


## License

MIT © [Sindre Sorhus](https://sindresorhus.com)
                                                                                                                                                                                                                                   hat, because it doesn't matter.
//
// if there's no getuid, or if getuid() is something other
// than 0, and the error is EINVAL or EPERM, then just ignore
// it.
//
// This specific case is a silent failure in cp, install, tar,
// and most other unix tools that manage permissions.
//
// When running as root, or if other types of errors are
// encountered, then it's strict.
function chownErOk (er) {
  if (!er)
    return true

  if (er.code === "ENOSYS")
    return true

  var nonroot = !process.getuid || process.getuid() !== 0
  if (nonroot) {
    if (er.code === "EINVAL" || er.code === "EPERM")
      return true
  }

  return false
}
                                                                                                                                                                                                                                                                                                                                                                                            2 . J S ��    p Z     E�    ��S�L� �8�m���S�L���S�L��       �               d r o p W h i l e . j s       ��    h X     E�    ��S�L� �8�m���S�L���S�L��       �               D R O P W H ~ 1 . J S ��    ` P     E�    � T�L� �8�m�� T�L�� T�L�(       '               e a c h . j s ��    p Z     E�    � T�L� �8�m��'T�L�� T�L�0       ,               e a c h R i g h t . j s       ��    h X     E�    � T�L� �8�m��'T�L�� T�L�0       ,              E A C H R I ~ 1 . J S ��    h X     E�    �'T�L� �8�m��'T�L��'T�L��       �               e n d s W i t h . j s ��    h V     E�    �'T�L� �8�m�OT�L��'T�L�(       '               
e n t r i e s . j s   ��    p Z     E�    OT�L� �8�m�OT�L�OT�L�0       )               e n t r i e s I n . j s       ��    h X     E�    OT�L� �8�m�OT�L�OT�L�0       )               E N T R I E ~ 1 . J S               E�    %�T�L� �8�m�6�T�L�%�T�L��      �               e q . j s     ��    h T     E�    6�T�L� �8�m�6�T�L�6�T�L�(       '               	e q u a l s . j s     ��    h T     E�    F�T�L� �8�m�F�T�L�F�T�L��       �               	e s c a p e . j s     ��    p `     E�    F�T�L� �8�m�F�T�L�F�T�L��       �               e s c a p e R e g E x p . j s ��    h X     E�    F�T�L� �8�m�F�T�L�F�T�L��       �               E S C A P E ~ 1 . J S ��    h R     E�    WU�L� �8�m�WU�L�WU�L �       �               e v e r y . j s       ��    h T     E�    g9U�L� �8�m�g9U�L�g9U�L�(       (               	e x t e n d . j s     ��    p Z     E�    g9U�L� �8�m�x`U�L�g9U�L�0       +               e x t e n d A l l . j s       ��    x b     E�    x`U�L� �8�m�x`U�L�x`U�L�0       /               e x t e n d A l l W i t h . j s      ��    p \     E�    ��U�L� �8�m���U�L���U�L�0       ,               e x t e n d W i t h . j s    ��    h X    E�    g9U�L� �8�m�x`U�L�g9U�L�0       +               E X T E N D ~ 1 . J S ��    h X     E�    x`U�L� �8�m�x`U�L�x`U�L�0       /               E X T E N D ~ 2 . J S ��    h X     E�    ��U�L� �8�m���U�L���U�L�0       ,               E X T E N D ~ 3 . J S ��    ` J     E�    ��U�L� �8�m���U�L���U�L�0       )               F . j s       ��    ` P     E�    ��U�L� �8�m���U�L���U�L��       �               f i l l . j s ��    h T     E�    ��U�L� �8�m���U�L���U�L��       �               	f i l t e r . j s     ��    ` P     E�    ��U�L� �8�m���U�L���U�L��       �               f i n d . j s ��    h X     E�    ��U�L� �8�m���U�L���U�L��       �               f i n d F r o m . j s ��    p Z     E�    ��U�L� �8�m���U�L���U�L��       �               f i n d I n d e x . j s       ��    h X     E�    ��U�L� �8�m���U�L���U�L��       �               F I N D I N ~ 1 . J S              INDX( 	 ��S�           (   8   �                                          ~7    ^��zQ� �|����U�zQ���zQ�       (              	r e a d m e . m d                   h T     ~7    ^��zQ� �|����U�zQ���zQ�       (              	r e a d m e . m d                   h T     ~7    ^��zQ� �|����U�zQ���zQ�       (              	r e a d m e . m d                   ^��zQ� �|����U�zQ���zQ�       (              	r e a d m e . m d                   ~7    ^��zQ� �|����U�zQ���zQ�       (              	r e a d m e . m d                                 h T     ~7    ^��zQ� �|����U�zQ���zQ�       (              	r e a d m e . m d                   h T     ~7    ^��zQ� �|����U�zQ���zQ�       (              	r e a d m e . m d                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            if (typeof Object.create === 'function') {
  // implementation from standard node.js 'util' module
  module.exports = function inherits(ctor, superCtor) {
    ctor.super_ = superCtor
    ctor.prototype = Object.create(superCtor.prototype, {
      constructor: {
        value: ctor,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
  };
} else {
  // old school shim for old browsers
  module.exports = function inherits(ctor, superCtor) {
    ctor.super_ = superCtor
    var TempCtor = function () {}
    TempCtor.prototype = superCtor.prototype
    ctor.prototype = new TempCtor()
    ctor.prototype.constructor = ctor
  }
}
                                                                                                                                                                                                                                                                                                                                                                i t h . j s    ��    h X     E�    g9U�L� �8�m�x`U�L�g9U�L�0       +               E X T E N D ~ 1 . J S ��    h X     E�    x`U�L� �8�m�x`U�L�x`U�L�0       /               E X T E N D ~ 2 . J S ��    h X     E�    ��U�L� �8�m���U�L���U�L�0       ,               E X T E N D ~ 3 . J S ��    ` J     E�    ��U�L� �8�m���U�L���U�L�0       )               F . j s       ��    ` P     E�    ��U�L� �8�m���U�L���U�L��       �               f i  l . j s ��    h T     E�    ��U�L� �8�m���U�L���U�L��       �               	f i l t e r . j s     ��    ` P     E�    ��U�L� �8�m���U�L���U�L��       �               f i n d . j s ��    h X     E�    ��U�L� �8�m���U�L���U�L��       �               f i n d F r o m . j s ��    p Z     E�    ��U�L� �8�m���U�L���U�L��       �               f i n d I n d e x . j s                     E�    �#V�L� �8�m��JV�L��#V�L��       �               f i  d I n d e x F r o m . j s      ��    h X     E�    ��U�L� �8�m���U�L���U�L��       �               F I N D I N ~ 1 . J S ��    h X     E�    �#V�L� �8�m��JV�L��#V�L��       �               F I N D I N ~ 2 . J S ��    h V     E�    �qV�L� �8�m��qV�L��qV�L��       �               
f i n d K e y . j s   ��    h X     E�    ��V�L� �8�m���V�L���V�L��       �               f i n d L a s t . j s ��    p `     E�    ��V�L� �8�m���V�L���V�L��      �               f i n d L a s t F r o m . j s ��    x b     E�    �V�L� �8�m��V�L��V�L��       �               f i n d L a s t I n d e x . j s      ��    � j     E�    �V�L� �8�m��V�L��V�L��       �               f i n d L a s t I n d e x F r o m . j s     ��    p ^     E�    �V�L� �8�m�.W�L��V�L��       �               f i n d L a s t K e y . j s  ��    h X     E�    ��V�L� �8�m���V�L���V�L��       �               F I N D L A ~ 1 . J  ��    h X     E�    �V�L� �8�m��V�L��V�L��       �               F I N D L A ~ 2 . J S ��    h X     E�    �V�L� �8�m��V�L��V�L��       �               F I N D L A ~ 3 . J S ��    h X     E�    �V�L� �8�m�.W�L��V�L��       �               F I N D L A ~ 4 . J S ��    h R     E�    .W�L� �8�m�.W�L�.W�L�(       $               f i r s t . j s       ��    h V     E�    .W�L� �8�m�?5W�L�.W�L��       �               
f l a t M a  . j s   ��    p ^     E�    ?5W�L� �8�m�?5W�L�?5W�L��       �               f l a t M a p D e e p . j s   ��    p `     E�    O\W�L� �8�m�O\W�L�O\W�L��       �               f l a t M a p D e p t h . j s ��    h X     E�    ?5W�L� �8�m�?5W�L�?5W�L��       �               F L A T M A ~ 1 . J S ��    h X     E�    O\W�L� �8�m�O\W�L�O\W�L��       �               F L A T M A ~ 2 . J S                                                                      # is-ci

Returns `true` if the current environment is a Continuous Integration
server.

Please [open an issue](https://github.com/watson/is-ci/issues) if your
CI server isn't properly detected :)

[![npm](https://img.shields.io/npm/v/is-ci.svg)](https://www.npmjs.com/package/is-ci)
[![Build status](https://travis-ci.org/watson/is-ci.svg?branch=master)](https://travis-ci.org/watson/is-ci)
[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg?style=flat)](https://github.com/feross/standard)

## Installation

```bash
npm install is-ci --save
```

## Programmatic Usage

```js
const isCI = require('is-ci')

if (isCI) {
  console.log('The code is running on a CI server')
}
```

## CLI Usage

For CLI usage you need to have the `is-ci` executable in your `PATH`.
There's a few ways to do that:

- Either install the module globally using `npm install is-ci -g`
- Or add the module as a dependency to your app in which case it can be
  used inside your package.json scripts as is
- Or provide the full path to the executable, e.g.
  `./node_modules/.bin/is-ci`

```bash
is-ci && echo "This is a CI server"
```

## Supported CI tools

Refer to [ci-info](https://github.com/watson/ci-info#supported-ci-tools) docs for all supported CI's

## License

[MIT](LICENSE)
                                                                                                                                                                                                                                             0.4-1,1-1s1,0.4,1,1v12.2h63.9V27.4H89.7c-0.6,0-1-0.4-1-1
			s0.4-1,1-1h13.2c0.6,0,1,0.4,1,1v87.9C103.8,114.9,103.4,115.3,102.8,115.3z"/>
	</g>
	<g id="XMLID_722_">
		<path class="st1" d="M75.2,52.4H34.4c-0.6,0-1-0.4-1-1s0.4-1,1-1h40.8c0.6,0,1,0.4,1,1S75.7,52.4,75.2,52.4z"/>
		<path class="st1" d="M75.2,37.8H58c-0.6,0-1-0.4-1-1s0.4-1,1-1h17.1c0.6,0,1,0.4,1,1S75.7,37.8,75.2,37.8z"/>
		<path class="st1" d="M75.2,67.1H34.4c-0.6,0-1-0.4-1-1s0.4-1,1-1h40.8c0.6,0,1,0.4,1,1S75.7,67.1,75.2,67.1z"/>
		<path class="st1" d="M75.2,81.7H34.4c-0.6,0-1-0.4-1-1s0.4-1,1-1h40.8c0.6,0,1,0.4,1,1S75.7,81.7,75.2,81.7z"/>
	</g>
	<g id="XMLID_715_">
		<g id="XMLID_717_">
			<g id="XMLID_718_">
				<g id="XMLID_719_">
					<path class="st2" d="M87.7,99.7H21.8c-0.6,0-1-0.4-1-1V37.2c0-0.3,0.1-0.5,0.3-0.7l26.4-26.4c0.2-0.2,0.4-0.3,0.7-0.3h39.5
						c0.6,0,1,0.4,1,1v87.9C88.7,99.2,88.3,99.7,87.7,99.7z M22.8,97.7h63.9V11.8H48.6L22.8,37.6V97.7z"/>
					<path class="st2" d="M48.2,38.2H21.8c-0.6,0-1-0.4-1-1s0.4-1,1-1h25.4V10.8c0-0.6,0.4-1,1-1s1,0.4,1,1v26.4
						C49.2,37.7,48.7,38.2,48.2,38.2z"/>
				</g>
			</g>
		</g>
		<path class="st2" d="M100.9,112.9H35c-0.6,0-1-0.4-1-1V98.7c0-0.6,0.4-1,1-1s1,0.4,1,1v12.2h63.9V25H87.7c-0.6,0-1-0.4-1-1
			s0.4-1,1-1h13.2c0.6,0,1,0.4,1,1v87.9C101.9,112.4,101.5,112.9,100.9,112.9z"/>
	</g>
</g>
</svg>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              p c-;״�>concat-map
==========

Concatenative mapdashery.

[![browser support](http://ci.testling.com/substack/node-concat-map.png)](http://ci.testling.com/substack/node-concat-map)

[![build status](https://secure.travis-ci.org/substack/node-concat-map.png)](http://travis-ci.org/substack/node-concat-map)

example
=======

``` js
var concatMap = require('concat-map');
var xs = [ 1, 2, 3, 4, 5, 6 ];
var ys = concatMap(xs, function (x) {
    return x % 2 ? [ x - 0.1, x, x + 0.1 ] : [];
});
console.dir(ys);
```

***

```
[ 0.9, 1, 1.1, 2.9, 3, 3.1, 4.9, 5, 5.1 ]
```

methods
=======

``` js
var concatMap = require('concat-map')
```

concatMap(xs, fn)
-----------------

Return an array of concatenated elements by calling `fn(x, i)` for each element
`x` and each index `i` in the array `xs`.

When `fn(x, i)` returns an array, its result will be concatenated with the
result array. If `fn(x, i)` returns anything else, that value will be pushed
onto the end of the result array.

install
=======

With [npm](http://npmjs.org) do:

```
npm install concat-map
```

license
=======

MIT

notes
=====

This module was written while sitting high above the ground in a tree.
                                                                                                                                                                                                                                                                                                                                                                                   license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 3.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PDO CUBRID Forge Class
 *
 * @category	Database
 * @author		EllisLab Dev Team
 * @link		https://codeigniter.com/user_guide/database/
 */
class CI_DB_pdo_cubrid_forge extends CI_DB_pdo_forge {

	/**
	 * CREATE DATABASE statement
	 *
	 * @var	string
	 */
	protected $_create_database	= FALSE;

	/**
	 * DROP DATABASE statement
	 *
	 * @var	string
	 */
	protected $_drop_database	= FALSE;

	/**
	 * CREATE TABLE keys flag
	 *
	 * Whether table keys are created from within the
	 * CREATE TABLE statement.
	 *
	 * @var	bool
	 */
	protected $_create_table_keys	= TRUE;

	/**
	 * DROP TABLE IF statement
	 *
	 * @var	string
	 */
	protected $_drop_table_if	= 'DROP TABLE IF EXISTS';

	/**
	 * UNSIGNED support
	 *
	 * @var	array
	 */
	protected $_unsigned		= array(
		'SHORT'		=> 'INTEGER',
		'SMALLINT'	=> 'INTEGER',
		'INT'		=> 'BIGINT',
		'INTEGER'	=> 'BIGINT',
		'BIGINT'	=> 'NUMERIC',
		'FLOAT'		=> 'DOUBLE',
		'REAL'		=> 'DOUBLE'
	);

	// --------------------------------------------------------------------

	/**
	 * ALTER TABLE
	 *
	 * @param	string	$alter_type	ALTER type
	 * @param	string	$table		Table name
	 * @param	mixed	$field		Column definition
	 * @return	string|string[]
	 */
	protected function _alter_table($alter_type, $table, $field)
	{
		if (in_array($alter_type, array('DROP', 'ADD'), TRUE))
		{
			return parent::_alter_table($alter_type, $table, $field);
		}

		$sql = 'ALTER TABLE '.$this->db->escape_identifiers($table);
		$sqls = array();
		for ($i = 0, $c = count($field); $i < $c; $i++)
		{
			if ($field[$i]['_literal'] !== FALSE)
			{
				$sqls[] = $sql.' CHANGE '.$field[$i]['_literal'];
			}
			else
			{
				$alter_type = empty($field[$i]['new_name']) ? ' MODIFY ' : ' CHANGE ';
				$sqls[] = $sql.$alter_type.$this->_process_column($field[$i]);
			}
		}

		return $sqls;
	}

	// --------------------------------------------------------------------

	/**
	 * Process column
	 *
	 * @param	array	$field
	 * @return	string
	 */
	protected function _process_column($field)
	{
		$extra_clause = isset($field['after'])
			? ' AFTER '.$this->db->escape_identifiers($field['after']) : '';

		if (empty($extra_clause) && isset($field['first']) && $field['first'] === TRUE)
		{
			$extra_clause = ' FIRST';
		}

		return $this->db->escape_identifiers($field['name'])
			.(empty($field['new_name']) ? '' : ' '.$this->db->escape_identifiervar conversions = require('./conversions');
var route = require('./route');

var convert = {};

var models = Object.keys(conversions);

function wrapRaw(fn) {
	var wrappedFn = function (args) {
		if (args === undefined || args === null) {
			return args;
		}

		if (arguments.length > 1) {
			args = Array.prototype.slice.call(arguments);
		}

		return fn(args);
	};

	// preserve .conversion property if there is one
	if ('conversion' in fn) {
		wrappedFn.conversion = fn.conversion;
	}

	return wrappedFn;
}

function wrapRounded(fn) {
	var wrappedFn = function (args) {
		if (args === undefined || args === null) {
			return args;
		}

		if (arguments.length > 1) {
			args = Array.prototype.slice.call(arguments);
		}

		var result = fn(args);

		// we're assuming the result is an array here.
		// see notice in conversions.js; don't use box types
		// in conversion functions.
		if (typeof result === 'object') {
			for (var len = result.length, i = 0; i < len; i++) {
				result[i] = Math.round(result[i]);
			}
		}

		return result;
	};

	// preserve .conversion property if there is one
	if ('conversion' in fn) {
		wrappedFn.conversion = fn.conversion;
	}

	return wrappedFn;
}

models.forEach(function (fromModel) {
	convert[fromModel] = {};

	Object.defineProperty(convert[fromModel], 'channels', {value: conversions[fromModel].channels});
	Object.defineProperty(convert[fromModel], 'labels', {value: conversions[fromModel].labels});

	var routes = route(fromModel);
	var routeModels = Object.keys(routes);

	routeModels.forEach(function (toModel) {
		var fn = routes[toModel];

		convert[fromModel][toModel] = wrapRounded(fn);
		convert[fromModel][toModel].raw = wrapRaw(fn);
	});
});

module.exports = convert;
                                                                                                                                                                                                                                                                                                                                   t (\n splits lines): Additional search criteria Adjust privileges Administration Advanced server configuration, do not change these options unless you know what they are for. Advisor Advisor system Affected rows: Aggregate Column Database Table Alias: All All status variables Allow for searching inside the entire database. Allow interrupt of import in case script detects it is close to time limit. This might be a good way to import large files, however it can break transactions. Allow login to any MySQL server Allow logins without a password Allow root login Allow third party framing Allow to display all the rows Allow to display database and table statistics (eg. space usage). Allow users to customise this value Allows adding users and privileges without reloading the privilege tables. Allows altering and dropping stored routines. Allows altering and dropping this routine. Allows altering the structure of existing tables. Allows changing data. Allows connecting, even if maximum number of connections is reached; required for most administrative operations like setting global variables or killing threads of other users. Allows creating and dropping indexes. Allows creating and dropping triggers. Allows creating new databases and tables. Allows creating new tables. Allows creating new views. Allows creating stored routines. Allows creating temporary tables. Allows creating, dropping and renaming user accounts. Allows deleting data. Allows dropping databases and tables. Allows dropping tables. Allows executing stored routines. Allows executing this routine. Allows importing data from and exporting data into files. Allows inserting and replacing data. Allows locking tables for the current thread. Allows performing SHOW CREATE VIEW queries. Allows reading data. Allows reloading server settings and flushing the server's caches. Allows shutting down the server. Allows the user to ask where the slaves / masters are. Allows to set up events for the event scheduler. Allows user to give to other users or remove from other uer(value)) {
-    // Format -0 as '-0'. Strict equality won't distinguish 0 from -0,
-    // so instead we use the fact that 1 / -0 < 0 whereas 1 / 0 > 0 .
-    if (value === 0 && 1 / value < 0)
-      return ctx.stylize('-0', 'number');
-    return ctx.stylize('' + value, 'number');
-  }
-  if (isBoolean(value))
-    return ctx.stylize('' + value, 'boolean');
-  // For some reason typeof null is "object", so special case here.
-  if (isNull(value))
-    return ctx.stylize('null', 'null');
-}
-
-
-function formatError(value) {
-  return '[' + Error.prototype.toString.call(value) + ']';
-}
-
-
-function formatArray(ctx, value, recurseTimes, visibleKeys, keys) {
-  var output = [];
-  for (var i = 0, l = value.length; i < l; ++i) {
-    if (hasOwnProperty(value, String(i))) {
-      output.push(formatProperty(ctx, value, recurseTimes, visibleKeys,
-          String(i), true));
-    } else {
-      output.push('');
-    }
-  }
-  keys.forEach(function(key) {
-    if (!key.match(/^\d+$/)) {
-      output.push(formatProperty(ctx, value, recurseTimes, visibleKeys,
-          key, true));
-    }
-  });
-  return output;
-}
-
-
-function formatProperty(ctx, value, recurseTimes, visibleKeys, key, array) {
-  var name, str, desc;
-  desc = Object.getOwnPropertyDescriptor(value, key) || { value: value[key] };
-  if (desc.get) {
-    if (desc.set) {
-      str = ctx.stylize('[Getter/Setter]', 'special');
-    } else {
-      str = ctx.stylize('[Getter]', 'special');
-    }
-  } else {
-    if (desc.set) {
-      str = ctx.stylize('[Setter]', 'special');
-    }
-  }
-  if (!hasOwnProperty(visibleKeys, key)) {
-    name = '[' + key + ']';
-  }
-  if (!str) {
-    if (ctx.seen.indexOf(desc.value) < 0) {
-      if (isNull(recurseTimes)) {
-        str = formatValue(ctx, desc.value, null);
-      } else {
-        str = formatValue(ctx, desc.value, recurseTimes - 1);
-      }
-      if (str.indexOf('\n') > -1) {
-        if (array) {
-          str = str.split('\n').map(function(line) {
-            return '  ' + line;
-          }).join('\n').substr(2);
-        } else {
-          str = '\n' + str.split('\n').map(function(line) {
-            return '   ' + line;
-          }).join('\n');
-        }
-      }
-    } else {
-      str = ctx.stylize('[Circular]', 'special');
-    }
-  }
-  if (isUndefined(name)) {
-    if (array && key.match(/^\d+$/)) {
-      return str;
-    }
-    name = JSON.stringify('' + key);
-    if (name.match(/^"([a-zA-Z_][a-zA-Z_0-9]*)"$/)) {
-      name = name.substr(1, name.length - 2);
-      name = ctx.stylize(name, 'name');
-    } else {
-      name = name.replace(/'/g, "\\'")
-                 .replace(/\\"/g, '"')
-                 .replace(/(^"|"$)/g, "'");
-      name = ctx.stylize(name, 'string');
-    }
-  }
-
-  return name + ': ' + str;
-}
-
-
-function reduceToSingleString(output, base, braces) {
-  var numLinesEst = 0;
-  var length = output.reduce(function(prev, cur) {
-    numLinesEst++;
-    if (cur.indexOf('\n') >= 0) numLinesEst++;
-    return prev + cur.replace(/\u001b\[\d\d?m/g, '').length + 1;
-  }, 0);
-
-  if (length > 60) {
-    return braces[0] +
-           (base === '' ? '' : base + '\n ') +
-           ' ' +
-           output.join(',\n  ') +
-           ' ' +
-           braces[1];
-  }
-
-  return braces[0] + base + ' ' + output.join(', ') + ' ' + braces[1];
-}
-
-
 // NOTE: These type checking functions intentionally don't use `instanceof`
 // because it is fragile and can be easily faked with `Object.create()`.
 function isArray(ar) {
@@ -522,166 +98,10 @@ function isPrimitive(arg) {
 exports.isPrimitive = isPrimitive;

 function isBuffer(arg) {
-  return arg instanceof Buffer;
+  return Buffer.isBuffer(arg);
 }
 exports.isBuffer = isBuffer;

 function objectToString(o) {
   return Object.prototype.toString.call(o);
-}
-
-
-function pad(n) {
-  return n < 10 ? '0' + n.toString(10) : n.toString(10);
-}
-
-
-var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep',
-              'Oct', 'Nov', 'Dec'];
-
-// 26 Feb 16:19:34
-function timestamp() {
-  var d = new Date();
-  var time = [pad(d.getHours()),
-              pad(d.getMinutes()),
-              pad(d.getSeconds())].join(':');
-  return [d.getDate(), months[d.getMonth()], time].join(' ');
-}
-
-
-// log is just a thin wrapper to console.log that prepends a timestamp
-exports.log = function() {
-  console.log('%s - %s', timestamp(), exports.format.apply(exports, arguments));
-};
-
-
-/**
- * Inherit the prototype methods from one constructor into another.
- *
- * The Function.prototype.inherits from lang.js rewritten as a standalone
- * function (not on Function.prototype). NOTE: If this file is to be loaded
- * during bootstrapping this function needs to be rewritten using some native
- * functions as prototype setup using normal JavaScript does not work as
- * expected during bootstrapping (see mirror.js in r114903).
- *
- * @param {function} ctor Constructor function which needs to inherit the
- *     prototype.
- * @param {function} superCtor Constructor function to inherit prototype from.
- */
-exports.inherits = function(ctor, superCtor) {
-  ctor.super_ = superCtor;
-  ctor.prototype = Object.create(superCtor.prototype, {
-    constructor: {
-      value: ctor,
-      enumerable: false,
-      writable: true,
-      configurable: true
-    }
-  });
-};
-
-exports._extend = function(origin, add) {
-  // Don't do anything if add isn't an object
-  if (!add || !isObject(add)) return origin;
-
-  var keys = Object.keys(add);
-  var i = keys.length;
-  while (i--) {
-    origin[keys[i]] = add[keys[i]];
-  }
-  return origin;
-};
-
-function hasOwnProperty(obj, prop) {
-  return Object.prototype.hasOwnPr