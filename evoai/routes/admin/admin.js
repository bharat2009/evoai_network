var express = require('express');
var router = express.Router();
var axios = require('axios');
var request = require('request');
var bodyParser = require('body-parser');
const session = require('express-session');
var md5 = require('md5');
const mysql = require('mysql');
const con = mysql.createConnection({
	host: 'localhost',
	user: 'root',
	password: '',
	database: 'evoai'
});

router.use(session({ 
	secret: 'somerandonstuffs',
	resave: false,
	saveUninitialized: false,
	cookie: { expires: 600000 }
  }));

/* GET home page. */
router.get('/', function(req, res, next) {
	var email_id = req.session.email_id;
	var admin_name = req.session.admin_name;
	if(email_id){
		res.redirect('/dashboard');		
	}
	else{
		res.render('admin/index', { title: 'Login' });
	}
});
/* Login */
router.post('/login', function(req, res, next) {
	var username = req.body.username;
	var password = md5(req.body.password);
	var sql = "SELECT * FROM admin WHERE admin_email='"+username+"' and admin_password='"+password+"'";
	con.query(sql, function (err2, result){
		if(err2) throw err2;
		else{					 
			if(result.length > 0){
				//console.log(result);
				req.session.email_id = result[0].admin_email;
				req.session.admin_name = result[0].admin_name;
				
				res.redirect('/dashboard');						
			}
			else{
				res.redirect('/');	
			}						
		}
	});		
});

/* Dashboard. */
router.get('/dashboard', function(req, res, next) {	
	var email_id = req.session.email_id;
	var admin_name = req.session.admin_name;
	if(email_id){
		res.render('dashboard', { title: 'Dashboard' });		
	}
	else{
		res.redirect('/');		
	}
});

/* Logout */
router.get("/logout", (req,res) => {
	req.session.destroy()
	res.redirect('/');
});

module.exports = router;
