var express = require('express');
var router = express.Router();
const session = require('express-session');
const bodyParser = require('body-parser');
var ip = require('ip');
const LoginURL = '/';
//const LoginURL = 'http://localhost/evoai/login';

const mysql = require('mysql');
const con = mysql.createConnection({
	host: 'localhost',
	user: 'root',
	password: '',
	database: 'evoai'
});

router.use(session({ 
	secret: 'somerandonstuffs',
	resave: false,
	saveUninitialized: false,
	cookie: { expires: 600000 }
  }));
/* GET home page. */
router.get('/', function(req, res, next) {	
	var userID = req.session.ID;
	if(userID)
	{
		res.render('dashboard', { title: 'Dashboard' });		
	}
	else
	{
		res.redirect('http://localhost/evoai/login');		
	}
	res.redirect(LoginURL);
});

/* Dashboard */
router.get('/dashboard', function(req, res, next) {
	var userID = req.session.email;
	if(userID){
		var sql = 'select * from users where id="'+userID+'"';
		con.query(sql, function (err, recordset) {
			if(recordset.length > 0){			
				if (err) 
					console.log(err)
				else
				{		
					res.render('dashboard', { title: 'Dashboard', links: recordset[0].user_referral_code });				
				}
			}
			else{
				res.redirect(LoginURL);							
			}
		});	
	}
	else{
		res.redirect(LoginURL);							
	}
});

/* Login */
router.post('/login', function(req, res, next) {
	//let id = req.query.a;
	let id = req.body.uId;
	var sql = 'select * from users where id="'+id+'"';
	con.query(sql, function (err, recordset) {
		if(recordset.length > 0){			
			if (err) 
				console.log(err)
			else
			{
				req.session.ID = recordset[0].id;
				req.session.email = recordset[0].email;
				res.redirect('/dashboard');			
			}
		}
		else{
			res.redirect(LoginURL);							
		}
	});
});

/* wallet */
router.get('/wallet', function(req, res, next) {	
	var userID = req.session.ID;
	if(userID)
	{
		res.render('wallet', { title: 'Wallet' });		
	}
	else
	{
		res.redirect(LoginURL);		
	}
});

/* live_trades */
router.get('/live_trades', function(req, res, next) {	
	var userID = req.session.ID;
	if(userID)
	{
		res.render('live_trades', { title: 'Live trades' });		
	}
	else
	{
		res.redirect(LoginURL);		
	}
});

/* evabot */
router.get('/evabot', function(req, res, next) {	
	var userID = req.session.ID;
	if(userID)
	{
		res.render('evabot', { title: 'Evabots' });		
	}
	else
	{
		res.redirect(LoginURL);		
	}
});

/* evobot */
router.get('/evobot', function(req, res, next) {	
	var userID = req.session.ID;
	if(userID)
	{
		res.render('evobot', { title: 'Evobot' });		
	}
	else
	{
		res.redirect(LoginURL);		
	}
});

/* eve */
router.get('/eve', function(req, res, next) {	
	var userID = req.session.ID;
	if(userID)
	{
		res.render('eve', { title: 'Eve' });		
	}
	else
	{
		res.redirect(LoginURL);		
	}
});

/* exchange */
router.get('/exchange', function(req, res, next) {	
	var userID = req.session.ID;
	if(userID)
	{
		res.render('exchange', { title: 'exchange' });		
	}
	else
	{
		res.redirect(LoginURL);		
	}
});

/* support */
router.get('/support', function(req, res, next) {	
	var userID = req.session.ID;
	if(userID)
	{
		res.render('support', { title: 'Support' });		
	}
	else
	{
		res.redirect(LoginURL);		
	}
});

/* support insert data */
router.post('/supportData', function(req, res, next) {	
	var userID = req.session.ID;
	if(userID)
	{
		var sql = "INSERT INTO support (user_id,subject,email,message) VALUES ('"+userID+"','"+req.body.subject+"','"+req.body.email+"','"+req.body.message+"')";
		con.query(sql, function (err2, result){
			if(err2) throw err2;
			res.redirect('/support');
		});
	}
	else
	{
		res.redirect(LoginURL);		
	}
});


/* support */
router.get('/myreferrals', function(req, res, next) {	
	var userID = req.session.ID;
	if(userID)
	{
		var sql = 'select * from users where id="'+userID+'"';
		con.query(sql, function (err, recordset) {
			if(recordset.length > 0){
				var userID = req.session.ID;
				if (err) 
					console.log(err)
				else
				{
					res.render('myreferrals', { title: 'My referrals', links: recordset[0].user_referral_code });
				}
			}
			else{
				res.redirect(LoginURL);							
			}
		});										
	}
	else
	{
		res.redirect(LoginURL);		
	}
});

/* announcements */
router.get('/announcements', function(req, res, next) {	
	var userID = req.session.ID;
	if(userID)
	{
		res.render('announcements', { title: 'Announcements' });		
	}
	else
	{
		res.redirect(LoginURL);		
	}
});

/* Logout */
router.get("/logout", (req,res) => {
	req.session.destroy()
	res.redirect(LoginURL);	
});
module.exports = router;
