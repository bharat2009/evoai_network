-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 25, 2018 at 04:30 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `evoai`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `admin_name` varchar(255) NOT NULL,
  `admin_username` varchar(255) NOT NULL,
  `admin_password` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_mobile_no` bigint(51) NOT NULL,
  `admin_active_inactive` tinyint(4) NOT NULL DEFAULT '1',
  `admin_created_date` date NOT NULL,
  `admin_modify_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_name`, `admin_username`, `admin_password`, `admin_email`, `admin_mobile_no`, `admin_active_inactive`, `admin_created_date`, `admin_modify_date`) VALUES
(1, 'admin', 'admin', '0192023a7bbd73250516f069df18b500', 'admin@gmail.com', 1, 1, '2018-08-08', '2018-08-08');

-- --------------------------------------------------------

--
-- Table structure for table `forgotpassword`
--

CREATE TABLE `forgotpassword` (
  `forgot_id` int(11) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_token` text NOT NULL,
  `created_date` varchar(255) NOT NULL,
  `CreateTime` varchar(255) NOT NULL,
  `ExpiryTime` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `support`
--

CREATE TABLE `support` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `subject` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `modify_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `support`
--

INSERT INTO `support` (`id`, `user_id`, `subject`, `email`, `message`, `modify_date`) VALUES
(2, 1, 'wert', 'bharatchhabra13@gmail.com', 'srt', '2018-09-24 19:45:42');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_fname` varchar(255) NOT NULL,
  `user_lname` varchar(255) NOT NULL,
  `user_image` varchar(255) DEFAULT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_referral_code` varchar(255) NOT NULL,
  `user_eth_address` text,
  `user_residence_country` varchar(255) DEFAULT NULL,
  `user_citizenship_country` varchar(255) DEFAULT NULL,
  `ip_address` varchar(15) DEFAULT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `otp` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `otp_login_code` varchar(42) DEFAULT NULL,
  `otp_backup_codes` varchar(384) DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `last_login` int(11) DEFAULT NULL,
  `isActive` tinyint(4) NOT NULL DEFAULT '1',
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `sign_in` tinyint(4) NOT NULL DEFAULT '1',
  `profile_update` tinyint(4) NOT NULL DEFAULT '1',
  `password_update` tinyint(4) NOT NULL DEFAULT '1',
  `transaction_approval` tinyint(4) NOT NULL DEFAULT '1',
  `mailSent` tinyint(4) DEFAULT NULL,
  `verify_status` tinyint(4) NOT NULL DEFAULT '0',
  `user_token` longtext,
  `ExpiryTime` varchar(245) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_fname`, `user_lname`, `user_image`, `user_email`, `user_password`, `user_referral_code`, `user_eth_address`, `user_residence_country`, `user_citizenship_country`, `ip_address`, `activation_code`, `otp`, `otp_login_code`, `otp_backup_codes`, `remember_code`, `salt`, `last_login`, `isActive`, `active`, `sign_in`, `profile_update`, `password_update`, `transaction_approval`, `mailSent`, `verify_status`, `user_token`, `ExpiryTime`) VALUES
(1, 'Bharat', '', '', 'om.brinfotech@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', '', '', '', 'PEfkBBMR5Di0x/tRnRCw+WdaT2jvdjw2hDk6R39T4W1PnsL7QdsV4kqh1/nTtcm3h1KCA8d1SMG4TejDbQULvQ==', '20X25vbW1XBJ/GsEWdnRf.23862b69620308ed83', 'qIoLeoTEJ67oFisWvpwfoYlGUcQ/+Kc+bZU1a3iLO2UYjur6fGqo9Y/5lnobT/VbuTudB6SyQTah9klCrMbeF38gJAWGefBzsir15mkFCdMiCs/Xxun4Gj5sjJoDZVCCQGYjbwjACKCZlu2xjCA4Pc/OXA9zEQqJh0Matgf2qUEwV2rM9qb9qhEP2IJjTQnFdpEaUjsDX7I9dqwWmS/XNYcWgenuNXFa9K+aGN7ABdPh4O4OzitWqEuqS76U9BewHaoJszDxHmwFKCASIV/s/NNMMWmCtJx3K5dV9Z18Up49ziNI7274gBzvFqBJqKxytTN9tlj44ARpWe1HKqruPTuVjhBtwwrxsu0b3g7zFoL3XqyHxGh29Neie3RnwszB', '', '', 0, 1, 0, 1, 0, 0, 0, 0, 1, '', ''),
(2, 'Bharat', 'chhabra', '', 'bharatchhabra13@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, '', ''),
(3, 'Invest101', '', '', 'lamhodang2548@hotmail.com', 'b83faadf0d3b9a031f99fb88789b84f1', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, '', ''),
(4, 'Bag', 'Collector', '', 'jmccool89@gmail.com', '3563c3b67b60f8cd02b6e7fc178b7407', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, '', ''),
(5, 'Milo_82', '', '', 'impexbooties@gmail.com', 'f9265b1c2f71a7396e0aae239c1a6bd6', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, '', ''),
(6, 'cryptoT', '', '', 'cryptoapparel4u@gmail.com', 'a7c332b383f22008b3471227a1812176', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, '', ''),
(7, 'JangoFett', '', '', 'brennie_08@live.com.au', '980d9499d2ddcf6fbd858e77be3cd893', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, '', ''),
(8, 'Alex Diaz', '', '', 'CryptoAlexDiazJr@gmail.com', 'b633ca0a43af4306913bb28614462c77', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, '', ''),
(9, 'KoF', '', '', 'HyperHyips@gmail.com', '90add6e39020f9156e90d64791ca1cec', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `otp` varchar(255) DEFAULT NULL,
  `otp_login_code` varchar(40) DEFAULT NULL,
  `otp_backup_codes` varchar(384) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `eth_address` varchar(255) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `sign_in` tinyint(4) NOT NULL DEFAULT '0',
  `active` tinyint(1) UNSIGNED DEFAULT '0',
  `verify_status` tinyint(1) NOT NULL DEFAULT '0',
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `user_referral_code` varchar(255) NOT NULL,
  `user_referenced_code` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `otp`, `otp_login_code`, `otp_backup_codes`, `salt`, `email`, `eth_address`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `sign_in`, `active`, `verify_status`, `first_name`, `last_name`, `company`, `phone`, `user_referral_code`, `user_referenced_code`) VALUES
(1, '', 'Bharat chhabra', 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, NULL, NULL, 'bharatchhabra13@gmail.com', '0x32Be343B94f860124dC4fEe278FDCBD38C102D86', NULL, NULL, NULL, NULL, 0, 1537266200, 0, 1, 1, NULL, NULL, NULL, NULL, '3ecfb8a8f5ada39497b1ca4358d1c47311b8057d', ''),
(2, '', 'Bharat chhabra', 'e10adc3949ba59abbe56e057f20f883e', 'sHYzmnY/prCkKkPEeOeeFjvswVSK4oKYLM6AjgD2G02HR5dlas7ZY8heeiPNmGUepIWFpio7LoGp9/kfYkAM4A==', 'V95OEj.EVlKp/KZAUhQKyu79a093c877c1dced25', 'pwfG74xRTiERD9zIx+qKOPzY+hr8DowO5w7Ag0eCC+2E1hxQUOgMMTMcL+jGsA04DTu0WEqkat+fzphQYelCH09Y6qLO+bgkJozb8KUM7ZlPoHn3K3ldxjHwc+IvHBmoQ4HxjFFkYtXtfc/FhJbhqkeRpFye1C0N0u2TgP8RMAZg5X0wLqSQ67ElZINY3lVGOYjfd0YNJ1+Iy7nK9mFFHDpq+mphwXdpmh9EOSEAl7udNz2o3coTvRAOZW9GS3e2hD0zsUHCTHhvDDcEuRpMD12dg6oCcaoVXIW9JKOvHgQTTsa9Ik16ehrjktvwAmv1hwhqK2Y3ZC5HQcaQLVVgJAXgZs1hSEFjPUSnOaqRtDGNYXzoWW5b4CL3pAYU2Bh9', NULL, 'bharatchhabra09@gmail.com', '', NULL, NULL, NULL, NULL, 0, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, '', ''),
(3, '', 'Invest101', 'b83faadf0d3b9a031f99fb88789b84f1', NULL, NULL, NULL, NULL, 'lamhodang2548@hotmail.com', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '', ''),
(4, '', 'Bag Collector', '3563c3b67b60f8cd02b6e7fc178b7407', NULL, NULL, NULL, NULL, 'jmccool89@gmail.com', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '', ''),
(5, '', 'Milo_82', 'f9265b1c2f71a7396e0aae239c1a6bd6', NULL, NULL, NULL, NULL, 'impexbooties@gmail.com', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '', ''),
(6, '', 'cryptoT', 'a7c332b383f22008b3471227a1812176', NULL, NULL, NULL, NULL, 'cryptoapparel4u@gmail.com', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '', ''),
(7, '', 'JangoFett', '980d9499d2ddcf6fbd858e77be3cd893', NULL, NULL, NULL, NULL, 'brennie_08@live.com.au', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '', ''),
(8, '', 'Alex Diaz', 'b633ca0a43af4306913bb28614462c77', NULL, NULL, NULL, NULL, 'CryptoAlexDiazJr@gmail.com', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '', ''),
(9, '', 'KoF', '90add6e39020f9156e90d64791ca1cec', NULL, NULL, NULL, NULL, 'HyperHyips@gmail.com', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '', ''),
(23, '', 'cryptostar', '4e1db8df84677d63682583d10b4a960e', NULL, NULL, NULL, NULL, 'cryptobeauty1.0@gmail.com', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '', ''),
(24, '', 'ETHER CLUB', '984bdda44d065bb6fe949e0b8f10e17f', NULL, NULL, NULL, NULL, 'ETHeRGAME2@protonmail.com', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '', ''),
(33, '', 'Joe', '3e0f7e4a44da1fe85f436e296a904343', NULL, NULL, NULL, NULL, 'castroj1987@icloud.com', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '3ecfb8a8f5ada39497b1ca4358d1c47311b8055d', ''),
(34, '', 'GeorgeN1', '17e0e4aea6cd6f3953206a6f57fd8fd5', NULL, NULL, NULL, NULL, 'geoneo@iafrica.com', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, 'a68026fc666384f098c8046c2dd0fc35b9f204b5', ''),
(35, '', 'tony', '7c3fe38a88ffa0a264c230c0e99c997e', NULL, NULL, NULL, NULL, 'morrisholding@orcon.net.nz', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '5cb9d93aab10db5667669a0efcd387f19cabfb02', ''),
(36, '', 'shanessy', '9235f17b604537ce3716e0a4bfcaf68b', NULL, 'PD/u5ni9OPyCn4LvDZE38Oc60e4ce48997b95fcc', '2ijgrMGQCP6DXNZNsJmZgs2hlhf0R4oM+ZnwhKtPXGV1fyg6Cy5w3CxbRcOFWnD0H1mQOp1WH3qVg/Y2PlW8ouph+Yuefwl6r3QG2maCNeIVYtlmeA2KlffAyk4hNC2Lf4X1ccvg0gfKtUZA8iIMboMDURO59RP+cFEUquiqM+Zwv40jtYycsth11fnbwf9I3x6MTOGSGB3wUQAYwnJ/Et5V2M1e3PVU7vqh9JT273qqX78HsLROK73CKPbVnoK3syMWuoEQagSFJO3Tp5pFcSt09kf1g6j9ZGV0JxRRBo6+yMsMe1kW7LVMG3NFHiKihsENQGNNjkvfChwYBP7ZHP+q7dlEaiTkBkmo8lG6UYAbdkIw+FuJmf7iphuSKq0k', NULL, 'shanessy11@gmail.com', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '67eb49fb28b3ce93eb414e31d3c6e0409d3b2e7d', ''),
(37, '', 'CryptoTiwi', 'f822bf4f1c962d5052bcde443afb8094', NULL, NULL, NULL, NULL, 'Cryptotiwi@gmail.com', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, 'a9592f622812af0d41cccfe116ad04264b90869c', '0'),
(38, '', 'Calais250', 'e45a4bbf258e0590bee162a349557dba', NULL, NULL, NULL, NULL, 'tamle250@yahoo.com.au', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '80a24a38dd4ecfb512d999d103e9cdd8b7b99026', '0'),
(39, '', 'Kasper Lundqvist', '804d4a425dc4aef7aa66577499c5f77d', NULL, NULL, NULL, NULL, 'alexnr1000@gmail.com', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, 'f27b895c82fd966f4c99b99ce38d1825128dd933', '0'),
(40, '', 'Andrew', '4d58d9fa6b555c8075c460f02d245724', NULL, NULL, NULL, NULL, 'newrossman@yahoo.com', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, '2429fb0d2a76d75f67a7ce3e88952374ba57996e', '0'),
(41, '', 'Gaia', 'b2daf1e200d020062940250c3b5fbef8', NULL, NULL, NULL, NULL, 'gaiaeconomy@gmail.com', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '62c424116c80ea72c432936ba10f8e79832eb10a', '0'),
(42, '', 'CHANDEL', '3ef745d422d389a287679f42100fdadd', NULL, NULL, NULL, NULL, 'mridul7070@gmail.com', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '49e63b3b54ae3dbfc01c4a117b780e14a0c3da82', '0'),
(43, '', 'oren730', '36cf869a174544a17b5b0b8a1e5eb8da', NULL, NULL, NULL, NULL, 'networking9@gmail.com', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '8deda04b3b4afb43e290e79f2fa5a4c9584bf3f1', '0'),
(44, '', 'sirpixalok', '9ac4c9a9b282a46b9afe10ae03acd6e6', NULL, NULL, NULL, NULL, 'rhodesn81@gmail.com', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '14ddc240b289dd831b9d269491d589fc9acb8723', '0');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 2),
(4, 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `user_inquiry`
--

CREATE TABLE `user_inquiry` (
  `id` int(11) NOT NULL,
  `order_type` varchar(50) NOT NULL,
  `currency_from_to` varchar(100) NOT NULL,
  `price` varchar(50) NOT NULL,
  `api_name` varchar(100) NOT NULL,
  `order_book_price` varchar(255) NOT NULL,
  `mix_order_book_price` varchar(255) NOT NULL,
  `calculate_price` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_inquiry`
--

INSERT INTO `user_inquiry` (`id`, `order_type`, `currency_from_to`, `price`, `api_name`, `order_book_price`, `mix_order_book_price`, `calculate_price`, `user_email`, `status`, `created_date`, `modify_date`) VALUES
(1, 'Buy', 'BTC_USDT', '0.01', 'BINANCE', '63.98030000', '63.97087629', '0.00942371', 'bharatchhabra13@gmail.com', 1, '2018-08-16 04:31:54', '2018-08-16 04:31:54'),
(2, 'Sell', 'ETH_BTC', '0.01', 'BITTREX', '0.00046100', '0.00046104', '0.00000004', 'bharatchhabra13@gmail.com', 1, '2018-08-16 04:46:06', '2018-08-16 04:46:06');

-- --------------------------------------------------------

--
-- Table structure for table `value_and_bonus`
--

CREATE TABLE `value_and_bonus` (
  `id` int(11) NOT NULL DEFAULT '1',
  `evot_value` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `bonus` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `modify_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `value_and_bonus`
--

INSERT INTO `value_and_bonus` (`id`, `evot_value`, `bonus`, `modify_date`) VALUES
(1, '0.35', '64', '01-09-2018');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `forgotpassword`
--
ALTER TABLE `forgotpassword`
  ADD PRIMARY KEY (`forgot_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `support`
--
ALTER TABLE `support`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- Indexes for table `user_inquiry`
--
ALTER TABLE `user_inquiry`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `forgotpassword`
--
ALTER TABLE `forgotpassword`
  MODIFY `forgot_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `support`
--
ALTER TABLE `support`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_inquiry`
--
ALTER TABLE `user_inquiry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
